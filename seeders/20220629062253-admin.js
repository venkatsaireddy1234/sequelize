"use strict";

module.exports = {
  async up(queryInterface, DataTypes) {
    await queryInterface.bulkInsert(
      "Roles",
      [
        {
          id: "2403ec51-17ea-40a6-9668-7ce996beb8de",
          name: "admin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: "17727de3-308d-4e5a-87ac-87739f36cee6",
          name: "Superadmin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: "d305695c-03a1-4dd9-9780-cc08abdfcb31",
          name: "Kitchenadmin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: "775ae622-f65f-4640-80e0-98aad380162f",
          name: "LogisticAdmin",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          id: "08372ac3-6c25-4a98-b98a-ff17b901d73a",
          name: "NutrituionAdmin",
          createdAt: new Date(),
          updatedAt: new Date(),  
        },
      ],
      {}
    );
  },

  async down(queryInterface, DataTypes) {
    await queryInterface.bulkDelete("Roles", null, {});
  },
};
