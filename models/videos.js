"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Videos extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Comments, {
        foreignKey: "commentableId",
        constraints: false,
        scope: {
          commentableType: "video",
        },
      });
    }
  }
  Videos.init(
    {
      url: DataTypes.STRING,
      duration: DataTypes.INTEGER,
    },
    {
      sequelize,
      modelName: "Videos",
    }
  );
  return Videos;
};
