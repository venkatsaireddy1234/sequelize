"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Comments extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Images, {
        foreignKey: "commentableId",
        constraints: false,
      });
      this.belongsTo(models.Videos, {
        foreignKey: "commentableId",
        constraints: false,
      });
    }
  }
  Comments.init(
    {
      text: DataTypes.STRING,
      commentableType: DataTypes.STRING,
      commentableId: DataTypes.UUID,
    },
    {
      sequelize,
      modelName: "Comments",
    }
  );
  return Comments;
};
