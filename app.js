const express = require("express");
const { json } = require("express/lib/response");
const res = require("express/lib/response");
const uuid = require("uuid");

const app = express();
app.use(express.json());
const {
  sequelize,
  User,
  Aadhar,
  Address,
  Roles,
  UserRoles,
  Images,
  Comments,
  Videos
} = require("./models");

app.post("/users", async (req, res) => {
  const { name, email, mobile } = req.body;

  try {
    const user = await User.create({ id: uuid.v4(), name, email, mobile });
    console.log("🚀 ~ file: app.js ~ line 13 ~ app.post ~ user", user);
    return res.json(user);
  } catch (err) {
    return res.status(500).json(err);
  }
});

app.get("/users", async (req, res) => {
  try {
    const users = await User.findAll();
    return res.json(users);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.get("/users/:id", async (req, res) => {
  const uuid = req.params.uuid;
  try {
    const users = await User.findOne({ where: { id: uuid } });
    return res.json(users);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.put("/users/:id", async (req, res) => {
  const uuid = req.params.uuid;
  console.log("🚀 ~ file: app.js ~ line 43 ~ app.put ~ uuid", uuid);
  const { name, email, mobile } = req.body;
  try {
    const user = await User.findOne({ where: { id: uuid } });
    console.log("🚀 ~ file: app.js ~ line 46 ~ app.put ~ user", user);
    user.name = name;
    user.email = email;
    user.mobile = mobile;
    await user.save();
    return res.json(user);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.delete("/users/:uuid", async (req, res) => {
  const uuid = req.params.uuid;
  try {
    const user = await User.destroy({ where: { id: uuid } });
    console.log(user);
    return res.json("successfully deleted the user");
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.post("/users/:id/aadhar", async (req, res) => {
  const { id } = req.params;
  //   console.log("🚀 ~ file: app.js ~ line 73 ~ app.post ~ uuid", uuid);
  const { aadharNumber, name } = req.body;
  const aadhar = await Aadhar.create({ id: uuid.v4(), aadharNumber, name });
  const user = await User.findOne({ where: { id: id } });
  user.aadharId = aadhar.id;
  await user.save();
  return res.json(user);
});

app.get("/users/:id/aadhar", async (req, res) => {
  const { id } = req.params;
  try {
    const user = await User.findOne({ where: { id: id } });
    console.log("🚀 ~ file: app.js ~ line 87 ~ app.get ~ user", user);
    const aadhar = await Aadhar.findOne({ where: { id: user.aadharId } });
    return res.json(aadhar);
  } catch (err) {
    console.log(err);
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.post("/users/:id/addresses", async (req, res) => {
  const { id } = req.params;
  const { name, street, city, country } = req.body;
  try {
    const address = await Address.create({
      id: uuid.v4(),
      name,
      street,
      city,
      userId: id,
      country,
    });
    console.log("🚀 ~ file: app.js ~ line 107 ~ app.post ~ address", address);
    return res.json(address);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.get("/users/:id/addresses", async (req, res) => {
  const { id } = req.params;
  try {
    const addresses = await Address.findAll({ where: { userId: id } });
    return res.json(addresses);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.get("/users/:id/addresses/:id", async (req, res) => {
  const { id } = req.params;
  console.log(id);
  try {
    const address = await Address.findOne({ where: { id: id } });
    return res.json(address);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.put("/users/:id/addresses/:id", async (req, res) => {
  const { id } = req.params;
  const { name, street, city, country } = req.body;

  try {
    const addressId = await Address.findOne({ where: { id: id } });
    addressId.name = name;
    addressId.street = street;
    addressId.city = city;
    addressId.country = country;
    await addressId.save();
    return res.json(addressId);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.post("/users/:userId/role/:roleId", async (req, res) => {
  const { userId, roleId } = req.params;
  try {
    const user = await User.findOne({ where: { id: userId } });
    const role = await Roles.findOne({ where: { id: roleId } });
    const userRole = await UserRoles.create({
      id: uuid.v4(),
      roleId: role.id,
      userId: user.id,
    });
    console.log(userRole);
    return res.json(userRole);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.get("/users/:userId/role/:roleId", async (req, res) => {
  const { roleId } = req.params;
  try {
    const userRoles = await UserRoles.findAll({ where: { roleId: roleId } });
    return res.json(userRoles);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.delete("/users/:userId/role/:roleId", async (req, res) => {
  const { userId, roleId } = req.params;
  try {
    const user = await User.findOne({ where: { id: userId } });
    const role = await Roles.findOne({ where: { id: roleId } });
    const userRoles = await UserRoles.findOne({
      where: { roleId: roleId, userId: userId },
    });
    await userRoles.destroy();
    return res.json("successfully deleted");
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.post("/images", async (req, res) => {
  const { url, height, width } = req.body;
  try {
    const image = await Images.create({ id: uuid.v4(), url, height, width });
    return res.json(image);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.post("/images/:id/comments", async (req, res) => {
  const { id } = req.params;
  const { text, commentableType } = req.body;
  try {
    const comments = await Comments.create({
      id: uuid.v4(),
      text,
      commentableId: id,
      commentableType,
    });
    console.log(comments);
    return res.json(comments);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.get("/images/:id/comments", async (req, res) => {
  const { id } = req.params;
  try {
    const comments = await Comments.findAll({ where: { commentableId: id } });
    return res.json(comments);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.post("/videos", async (req, res) => {
  const { url, duration } = req.body;
  try {
    const video = await Videos.create({ id: uuid.v4(), url, duration });
    return res.json(video);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.post("/videos/:id/comments", async (req, res) => {
  const { id } = req.params;
  const { text, commentableType } = req.body;
  try {
    const videocomment = await Comments.create({
      id: uuid.v4(),
      text,
      commentableId: id,
      commentableType,
    });
    console.log(videocomment);
    return res.json(videocomment);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});

app.get("/videos/:id/comments", async (req, res) => {
  const { id } = req.params;
  try {
    const comments = await Comments.findAll({ where: { commentableId: id } });
    return res.json(comments);
  } catch (err) {
    return res.status(500).json({ error: "something went wrong" });
  }
});
app.listen({ port: 5000 }, async () => {
  console.log("server up on localhost 5000");
  console.log("Database synced");
});
